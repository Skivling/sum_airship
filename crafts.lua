local S = minetest.get_translator(minetest.get_current_modname())


minetest.register_craftitem("sum_airship:canvas_roll", {
	description = S("Canvas Roll"),
	_doc_items_longdesc = S("Used in crafting airships."),
	inventory_image = "sum_airship_canvas.png",
	stack_max = 64,
	groups = { craftitem = 1 },
})
minetest.register_craftitem("sum_airship:hull", {
	description = S("Airship Hull"),
	_doc_items_longdesc = S("Used in crafting airships."),
	inventory_image = "sum_airship_hull.png",
	stack_max = 1,
	groups = { craftitem = 1 },
})


local w = "group:wool"
if not minetest.get_modpath("farming") then w = "default:paper" end
local b = "boats:boat"
local m = "default:steel_ingot"
local s = "farming:string"
if minetest.get_modpath("mcl_boats")
and minetest.get_modpath("mcl_wool")
and minetest.get_modpath("mcl_core")
and minetest.get_modpath("mcl_mobitems") then
	w = "group:wool"
	b = "group:boat"
	m = "mcl_core:iron_ingot"
	s = "mcl_mobitems:string"
end
local rp_fuzzy = minetest.get_modpath("rp_farming") or minetest.get_modpath("rp_mobs_mobs")
if rp_fuzzy and minetest.get_modpath("rp_default") then
	w = "group:fuzzy" -- cotton bale or wool bundle
	b = "group:boat"
	m = "rp_default:ingot_steel"
	s = "rp_default:fiber"
end

if minetest.get_modpath("rp_crafting") then
	crafting.register_craft({
		output = "sum_airship:canvas_roll",
		items = {
			w.." 9"
		}
	})
	crafting.register_craft({
		output = "sum_airship:hull",
		items = {
			b.." 3",
			m.." 3"
		}
	})
	crafting.register_craft({
		output = "sum_airship:boat",
		items = {
			"sum_airship:canvas_roll 3",
			s.." 8",
			m,
			"sum_airship:hull",
		}
	})
else
	minetest.register_craft({
		output = "sum_airship:canvas_roll",
		recipe = {
			{w, w, w},
			{w, w, w},
			{w, w, w},
		},
	})
	minetest.register_craft({
		output = "sum_airship:hull",
		recipe = {
			{b, b, b},
			{m, m, m},
		},
	})
	minetest.register_craft({
		output = "sum_airship:boat",
		recipe = {
			{"sum_airship:canvas_roll","sum_airship:canvas_roll","sum_airship:canvas_roll",},
			{s, m, s,},
			{s, "sum_airship:hull", s,},
		},
	})
end